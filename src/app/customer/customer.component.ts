import { Component, OnInit } from '@angular/core';
import {CustomerService} from '../customer.service';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  FirstName:string = "";
  LastName:string = "";
  public customers: any[];
  constructor(public service: CustomerService) { }
  onKey(){
     var customer ={
       FirstName: this.FirstName,
       LastName: this.LastName
     }
     this.customers.push(customer);
  }
  ngOnInit() {
    this.customers = this.service.getCustomer();
  }

}
